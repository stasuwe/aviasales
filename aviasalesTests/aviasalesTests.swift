//
//  aviasalesTests.swift
//  aviasalesTests
//
//  Created by Станислав Сарычев on 31/07/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import XCTest
@testable import aviasales
import RxTest
import RxSwift

class aviasalesTests: XCTestCase {
    let scheduler = TestScheduler(initialClock: 0)
    let bag = DisposeBag()

    func testTitleIsSet() {
        let (uiMock, observedState) = makeUIMockAndStateOrserver()

        let module = makeSearchModule(uiBindings: uiMock)

        _ = scheduler.start { module("title") }

        XCTAssertEqual(observedState.elements.last?.title, "title")
    }

    func testRequestPlaces() {
        let (uiMock, observedState) = makeUIMockAndStateOrserver(events: [.request("abc")])
        var requestString = ""

        let module = makeSearchModule(uiBindings: uiMock,
                                      places: { requestString = $0; return .just([.mock]) })

        _ = scheduler.start { module("") }

        XCTAssertEqual(requestString, "abc")
        XCTAssertEqual(observedState.elements.last?.places, [.mock])
    }

    func testPlaceSelected() {
        let (uiMock, _) = makeUIMockAndStateOrserver(events: [.request(""), .placeSelected(.mock)])

        let module = makeSearchModule(uiBindings: uiMock, places: { _ in .just([.mock]) })

        let result = scheduler.start { module("") }

        XCTAssertEqual(result.elements, [.place(.mock)])
    }
}

extension aviasalesTests {
    func makeSearchModule(uiBindings: @escaping SearchModule.FeedbackLoop = { _ in .empty() },
                          places: @escaping (String) -> Single<[Place]> = { _ in Observable.empty().asSingle() },
                          dismiss: @escaping () -> Void = {})
        -> SearchModule.Start {
            return SearchModule.make(uiBindnigs: uiBindings, places: places, dismiss: dismiss, scheduler: scheduler)
    }

    func makeUIMockAndStateOrserver(events: [SearchModule.Event] = [])
        -> (SearchModule.FeedbackLoop, TestableObserver<SearchModule.State>) {
            let observedState = scheduler.createObserver(SearchModule.State.self)
            return ({
                $0.bind(to: observedState).disposed(by: self.bag)
                return self.scheduler
                    .createColdObservable(events
                        .enumerated()
                        .map { Recorded.next(200 + $0*10, $1) })
                    .asObservable()
            }, observedState)
    }
}

extension TestableObserver {
    var elements: [Element] {
        return events.compactMap { $0.value.element }
    }
}

extension Place {
    static var mock: Place {
        return .init(iata: "abc", location: .mock, name: "Name")
    }
}

extension Location {
    static var mock: Location {
        return .init(lat: 0, lon: 0)
    }
}
