//
//  SearchModuleState.swift
//  aviasales
//
//  Created by Станислав Сарычев on 31/07/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

extension SearchModule {
    struct State {
        var title: String
        var request: String?
        var places: [Place] = []
        var selectedPlace: Place?
    }

    enum Event {
        case request(String)
        case placesLoaded([Place])
        case placeSelected(Place)
    }
}

extension SearchModule {
    static func reduce(state: SearchModule.State, event: SearchModule.Event) -> SearchModule.State {
        var mutableState = state

        switch event {
        case .request(let request):
            mutableState.request = request
        case .placeSelected(let place):
            mutableState.selectedPlace = place
        case .placesLoaded(let places):
            mutableState.places = places
        }

        return mutableState
    }
}
