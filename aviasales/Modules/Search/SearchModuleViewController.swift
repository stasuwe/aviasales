//
//  SearchModuleViewController.swift
//  aviasales
//
//  Created by Станислав Сарычев on 31/07/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import UIKit
import RxSwift
import RxFeedback
import RxDataSources

class SearchModuleViewController: UIViewController {
    private let _searchField = UITextField(frame: .zero)
    private let _tableView = UITableView(frame: .zero, style: .plain)

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white

        view.addSubview(_searchField)
        _searchField.borderStyle = .roundedRect
        _searchField.placeholder = "Where to go?"
        _searchField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            _searchField.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 16),
            _searchField.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            _searchField.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor)
            ])

        view.addSubview(_tableView)
        _tableView.translatesAutoresizingMaskIntoConstraints = false
        _tableView.register(UITableViewCell.self, forCellReuseIdentifier: "placeCell")
        NSLayoutConstraint.activate([
            _tableView.topAnchor.constraint(equalTo: _searchField.bottomAnchor, constant: 16),
            _tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            _tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            _tableView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor)
            ])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SearchModuleViewController {

    var bindings: SearchModule.FeedbackLoop{
        return bind(self) { me, state in
            
            let subscriptions = [
                state.map { $0.places }
                    .map { [SectionModel(model: "", items: $0)] }
                    .bind(to: me._tableView.rx.items(dataSource: me._dataSource)),
                state.map { $0.title }
                    .bind(to: me.rx.title)
            ]

            let events = [
                me._searchField.rx
                    .text.orEmpty
                    .debounce(0.5, scheduler: MainScheduler.instance)
                    .map(SearchModule.Event.request),
                me._tableView.rx
                    .modelSelected(Place.self)
                    .map(SearchModule.Event.placeSelected)
            ]

            return Bindings(subscriptions: subscriptions, events: events)
        }
    }

    private var _dataSource: RxTableViewSectionedReloadDataSource<SectionModel<String, Place>> {
        return .init(configureCell: { _, tableView, indexPath, item in
            let cell = tableView.dequeueReusableCell(withIdentifier: "placeCell", for: indexPath)
            cell.textLabel?.text = [item.name, item.iata].joined(separator: ", ")
            return cell
        })
    }
}
