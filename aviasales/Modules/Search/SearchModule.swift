//
//  SearchModule.swift
//  aviasales
//
//  Created by Станислав Сарычев on 31/07/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import RxSwift
import RxFeedback

enum SearchModule {
    typealias FeedbackLoop = (ObservableSchedulerContext<State>) -> Observable<Event>
    typealias Start = (String) -> Observable<Output>

    static func make(uiBindnigs: @escaping FeedbackLoop,
                     places: @escaping (String) -> Single<[Place]>,
                     dismiss: @escaping () -> Void,
                     scheduler: SchedulerType = MainScheduler.instance) -> Start {
        return { title in
            let loadPlaces: FeedbackLoop = react(request: { $0.request },
                                                 effects: { places($0)
                                                    .map(SearchModule.Event.placesLoaded)
                                                    .asObservable() })

            let dismiss: FeedbackLoop = react(request: { $0.selectedPlace },
                                              effects: { _ in dismiss(); return .empty() })
            return Observable
                .system(initialState: SearchModule.State(title: title, request: nil, places: [], selectedPlace: nil),
                        reduce: SearchModule.reduce,
                        scheduler: scheduler,
                        feedback: [uiBindnigs, loadPlaces, dismiss])
                .flatMap {
                    $0.selectedPlace
                        .map(Output.place)
                        .map(Observable.just) ?? .empty()
            }
        }
    }

    static func makeModule(show: @escaping (UIViewController) -> Void,
                           places: @escaping (String) -> Single<[Place]>) -> () -> Start {
        let viewController = SearchModuleViewController()
        return {
            show(viewController)
            return make(uiBindnigs: viewController.bindings,
                        places: places,
                        dismiss: { viewController.dismiss(animated: true) })
        }
    }
}

extension SearchModule {
    enum Output: Equatable {
        case place(Place)
    }
}
