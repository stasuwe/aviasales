//
//  MainModuleState.swift
//  aviasales
//
//  Created by Станислав Сарычев on 01/08/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

extension MapModule {
    struct State: Equatable {
        var searchRequest: SearchRequest
        var planeLocation: Location
        var geodesicPolyline: MKGeodesicPolyline?
        var planeAnnotationPosition: Int = 0
        var planeDirection: Double = 0
    }

    enum Event {
        case timerTick
        case geodesicPolyline(MKGeodesicPolyline)
    }
}

import MapKit

extension MapModule {
    static func reduce(state: MapModule.State, event: MapModule.Event) -> MapModule.State {
        var mutableState = state

        switch event {
        case .geodesicPolyline(let polyline):
            mutableState.geodesicPolyline = polyline
        case .timerTick:
            guard let polyline = state.geodesicPolyline,
                state.planeAnnotationPosition + 5 < polyline.pointCount else { break }

            let points = polyline.points()

            let prevPoint = points[mutableState.planeAnnotationPosition]
            mutableState.planeAnnotationPosition += 5
            let nextPoint = points[mutableState.planeAnnotationPosition]

            mutableState.planeDirection = (atan2(nextPoint.y - prevPoint.y,
                                                 nextPoint.x - prevPoint.x) * 180 / .pi)
                .truncatingRemainder(dividingBy: 360)
            mutableState.planeLocation = .init(lat: nextPoint.coordinate.latitude,
                                               lon: nextPoint.coordinate.longitude)
        }

        return mutableState
    }
}

extension MapModule.State {
    init(searchRequest: SearchRequest) {
        self.searchRequest = searchRequest
        planeLocation = searchRequest.from.location
        planeAnnotationPosition = 0
        planeDirection = 0
    }
}
