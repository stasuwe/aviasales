//
//  SearchModuleViewController.swift
//  aviasales
//
//  Created by Станислав Сарычев on 31/07/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import RxFeedback
import RxMKMapView

class MapModuleViewController: UIViewController {
    private let _mapView = MKMapView(frame: .zero)
    private let _planeAnnotation = MKPointAnnotation()
    private var _planeView: MKAnnotationView?
    private let _polyline = BehaviorSubject<MKGeodesicPolyline>(value: MKGeodesicPolyline(coordinates: [], count: 0))

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white

        view.addSubview(_mapView)
        _mapView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            _mapView.topAnchor.constraint(equalTo: view.topAnchor),
            _mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            _mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            _mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
        _mapView.register(PlaceAnnotationView.self, forAnnotationViewWithReuseIdentifier: "placeAnnotation")
        _mapView.register(MKAnnotationView.self, forAnnotationViewWithReuseIdentifier: "planeAnnotation")
        _mapView.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MapModuleViewController {

    var bindings: MapModule.FeedbackLoop{
        return bind(self) { me, state in
            let subscriptions = [
                state.map { $0.searchRequest }
                    .distinctUntilChanged()
                    .map {
                        [$0.from.location, $0.to.location]
                            .map { ($0.lat, $0.lon) }
                            .map(CLLocation.init)
                            .map { $0.coordinate }
                    }
                    .subscribe(onNext: {
                        me._mapView.addOverlay(MKGeodesicPolyline(coordinates: $0, count: $0.count))
                    }),
                state.map { $0.searchRequest }
                    .distinctUntilChanged()
                    .map {
                        [$0.from, $0.to]
                            .map { PlaceAnnotation(title: $0.iata,
                                                   coordinate: .init(latitude: $0.location.lat,
                                                                     longitude: $0.location.lon)) }
                    }
                    .subscribe(onNext: {
                        me._mapView.addAnnotations($0)
                        me._mapView.showAnnotations($0, animated: false)
                    }),
                state.map { $0.planeLocation }
                    .take(1)
                    .map {
                        me._planeAnnotation.coordinate = .init(latitude: $0.lat, longitude: $0.lon)
                        return me._planeAnnotation
                    }
                    .subscribe(onNext: me._mapView.addAnnotation),
                state
                    .distinctUntilChanged()
                    .subscribe(onNext: {
                        me._planeAnnotation.coordinate = .init(latitude: $0.planeLocation.lat,
                                                          longitude: $0.planeLocation.lon)
                        
                        me._planeView?.transform = CGAffineTransform(rotationAngle: CGFloat($0.planeDirection * .pi / 180))
                    })
            ]

            let events = [
                me._polyline.map(MapModule.Event.geodesicPolyline).debug()
            ]

            return Bindings(subscriptions: subscriptions, events: events)
        }
    }
}

extension MapModuleViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return (overlay as? MKGeodesicPolyline).map(_geodesicPolylineRenderer) ?? MKOverlayRenderer()
    }

    func mapView(_ mapView: MKMapView, didAdd renderers: [MKOverlayRenderer]) {
        _polyline.onNext(renderers.compactMap { $0.overlay as? MKGeodesicPolyline }.first!)
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        switch annotation {
        case is PlaceAnnotation:
            return mapView.dequeueReusableAnnotationView(withIdentifier: "placeAnnotation", for: annotation)
        case is MKPointAnnotation:
            _planeView = mapView.dequeueReusableAnnotationView(withIdentifier: "planeAnnotation", for: annotation)
            _planeView?.image = UIImage(named: "plane")
            return _planeView
        default:
            return nil
        }
    }

    private func _geodesicPolylineRenderer(_ polyline: MKPolyline) -> MKPolylineRenderer {
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.lineWidth = 3
        renderer.alpha = 0.5
        renderer.strokeColor = .blue
        renderer.lineDashPattern = [0, 5]
        return renderer
    }
}
