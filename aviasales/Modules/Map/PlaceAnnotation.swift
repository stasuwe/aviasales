//
//  PlaceAnnotation.swift
//  aviasales
//
//  Created by Станислав Сарычев on 04/08/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import MapKit

class PlaceAnnotation: NSObject, MKAnnotation {
    let coordinate: CLLocationCoordinate2D
    let title: String?

    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
    }
}
