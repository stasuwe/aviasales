//
//  PlaceAnnotationView.swift
//  aviasales
//
//  Created by Станислав Сарычев on 04/08/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import MapKit

class PlaceAnnotationView: MKAnnotationView {

    let _titleLabel = UILabel(frame: .zero)

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        frame = CGRect(x: 0, y: 0, width: 62, height: 25)

        backgroundColor = UIColor.white.withAlphaComponent(0.5)
        layer.cornerRadius = 13
        layer.masksToBounds = true
        layer.borderWidth = 2
        layer.borderColor = UIColor.blue.withAlphaComponent(0.5).cgColor

        addSubview(_titleLabel)
        _titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            _titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            _titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
            ])
        _titleLabel.textColor = UIColor.blue.withAlphaComponent(0.5)
        _titleLabel.font = .preferredFont(forTextStyle: .headline)
    }

    override func draw(_ rect: CGRect) {
        _titleLabel.text = annotation?.title ?? ""
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
