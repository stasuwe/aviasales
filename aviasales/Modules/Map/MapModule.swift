//
//  MainModule.swift
//  aviasales
//
//  Created by Станислав Сарычев on 31/07/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import RxSwift
import RxFeedback

enum MapModule {
    typealias FeedbackLoop = (ObservableSchedulerContext<State>) -> Observable<Event>
    typealias Start = (SearchRequest) -> Observable<Output>

    static func make(uiBindnigs: @escaping FeedbackLoop) -> Start {
        return { searchRequest in

            let timer: FeedbackLoop = react(request: { _ in true },
                                            effects: { _ in Observable<Int>.timer(0, period: 0.03,
                                                                                  scheduler: MainScheduler.instance)
                                                .map { _ in .timerTick } })

            return Observable
                .system(initialState: MapModule.State(searchRequest: searchRequest),
                        reduce: MapModule.reduce,
                        scheduler: MainScheduler.instance,
                        feedback: [uiBindnigs, timer])
                .asEmpty()
        }
    }

    static func makeModule(show: @escaping (UIViewController) -> Void) -> () -> Start {
        let viewController = MapModuleViewController()
        return {
            show(viewController)
            return make(uiBindnigs: viewController.bindings)
        }
    }
}

extension MapModule {
    enum Input {}
    enum Output {}
}
