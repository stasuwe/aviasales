//
//  MainModuleState.swift
//  aviasales
//
//  Created by Станислав Сарычев on 01/08/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

extension AppModule {
    struct State {
        var searchRequest: SearchRequest?
    }

    enum Event {
        case searchRequest(SearchRequest)
    }
}

extension AppModule {
    static func reduce(state: AppModule.State, event: AppModule.Event) -> AppModule.State {
        var mutableState = state

        switch event {
        case .searchRequest(let request):
            mutableState.searchRequest = request
        }

        return mutableState
    }
}
