//
//  MainModule.swift
//  aviasales
//
//  Created by Станислав Сарычев on 31/07/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import RxSwift
import RxFeedback

enum AppModule {
    typealias FeedbackLoop = (ObservableSchedulerContext<State>) -> Observable<Event>
    typealias Start = () -> Observable<Output>

    static func make(mainModule: @escaping () -> Observable<Input>,
                     mapModule: @escaping (SearchRequest) -> Observable<Input>) -> Start {
        return {

            let startMainModule: FeedbackLoop = react(request: { _ in true },
                                                      effects: { _ in mainModule().map { $0.searchRequest }
                                                        .map(AppModule.Event.searchRequest)
                                                        .asObservable() })

            let startMapModule: FeedbackLoop = react(request: { $0.searchRequest },
                                                     effects: { mapModule($0).asEmpty() })

            return Observable
                .system(initialState: AppModule.State(),
                        reduce: AppModule.reduce,
                        scheduler: MainScheduler.instance,
                        feedback: [startMainModule, startMapModule])
                .asEmpty()
        }
    }
}

extension AppModule {
    enum Input {
        case searchRequest(SearchRequest)
    }
    
    enum Output {}
}

extension AppModule.Input {
    var searchRequest: SearchRequest {
        switch self {
        case .searchRequest(let request):
            return request
        }
    }
}
