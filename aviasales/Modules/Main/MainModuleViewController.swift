//
//  SearchModuleViewController.swift
//  aviasales
//
//  Created by Станислав Сарычев on 31/07/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import UIKit
import RxSwift
import RxFeedback

class MainModuleViewController: UIViewController {
    private let _fromButton = UIButton(type: .roundedRect)
    private let _toButton = UIButton(type: .roundedRect)
    private let _searchButton = UIButton(type: .roundedRect)

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white

        view.addSubview(_fromButton)
        _fromButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            _fromButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -60),
            _fromButton.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            _fromButton.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor)
            ])

        view.addSubview(_toButton)
        _toButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            _toButton.topAnchor.constraint(equalTo: _fromButton.bottomAnchor, constant: 16),
            _toButton.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            _toButton.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor)
            ])

        view.addSubview(_searchButton)
        _searchButton.setTitle("Search", for: [])
        _searchButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            _searchButton.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor, constant: -16),
            _searchButton.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            _searchButton.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor)
            ])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MainModuleViewController {

    var bindings: MainModule.FeedbackLoop{
        return bind(self) { me, state in

            let subscriptions = [
                state.map { $0.from?.name ?? "Select from" }
                    .bind(to: me._fromButton.rx.title(for: [])),
                state.map { $0.to?.name ?? "Select to" }
                    .bind(to: me._toButton.rx.title(for: []))
            ]

            let events = [
                me._fromButton.rx
                    .tap
                    .map { _ in MainModule.Event.selectFromPlace },
                me._toButton.rx
                    .tap
                    .map { _ in MainModule.Event.selectToPlace },
                me._searchButton.rx
                    .tap
                    .map { _ in MainModule.Event.startSearch }
            ]

            return Bindings(subscriptions: subscriptions, events: events)
        }
    }
}
