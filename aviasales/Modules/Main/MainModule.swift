//
//  MainModule.swift
//  aviasales
//
//  Created by Станислав Сарычев on 31/07/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import RxSwift
import RxFeedback

enum MainModule {
    typealias FeedbackLoop = (ObservableSchedulerContext<State>) -> Observable<Event>
    typealias Start = () -> Observable<Output>

    static func make(uiBindnigs: @escaping FeedbackLoop,
                     place: @escaping (String) -> Observable<Input>) -> Start {
        return {
            let fromPlace: FeedbackLoop = react(request: { $0.direction == .from ? $0.direction?.rawValue : nil },
                                                effects: { place($0).map { $0.place }
                                                    .map(MainModule.Event.fromPlaceSelected)
                                                    .asObservable() })

            let toPlace: FeedbackLoop = react(request: { $0.direction == .to ? $0.direction?.rawValue : nil },
                                              effects: { place($0).map { $0.place }
                                                .map(MainModule.Event.toPlaceSelected)
                                                .asObservable() })

            return Observable
                .system(initialState: MainModule.State(),
                        reduce: MainModule.reduce,
                        scheduler: MainScheduler.instance,
                        feedback: [uiBindnigs, fromPlace, toPlace])
                .flatMap {
                    $0.searchRequest
                        .map(Output.searchRequest)
                        .map(Observable.just) ?? .empty()
                }
        }
    }

    static func makeModule(show: @escaping (UIViewController) -> Void,
                           place: @escaping (String) -> Observable<Input>) -> () -> Start {
        let viewController = MainModuleViewController()
        return {
            show(viewController)
            return make(uiBindnigs: viewController.bindings, place: place)
        }
    }
}

extension MainModule {
    enum Input {
        case place(Place)
    }

    enum Output {
        case searchRequest(SearchRequest)
    }
}


extension MainModule.Input {
    var place: Place {
        switch self {
        case .place(let place):
            return place
        }
    }
}
