//
//  MainModuleState.swift
//  aviasales
//
//  Created by Станислав Сарычев on 01/08/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

extension MainModule {
    struct State {
        var direction: Direction?
        var from: Place?
        var to: Place?
        var searchRequest: SearchRequest?

        enum Direction: String {
            case from = "From"
            case to = "To"
        }
    }

    enum Event {
        case selectFromPlace
        case fromPlaceSelected(Place)
        case selectToPlace
        case toPlaceSelected(Place)
        case startSearch
    }
}

extension MainModule {
    static func reduce(state: MainModule.State, event: MainModule.Event) -> MainModule.State {
        var mutableState = state

        switch event {
        case .selectFromPlace:
            mutableState.direction = .from
        case .fromPlaceSelected(let place):
            mutableState.from = place
            mutableState.direction = nil
        case .selectToPlace:
            mutableState.direction = .to
        case .toPlaceSelected(let place):
            mutableState.to = place
            mutableState.direction = nil
        case .startSearch:
            mutableState.searchRequest = zip(mutableState.from, mutableState.to).map(SearchRequest.init)
        }

        return mutableState
    }
}
