//
//  AppDelegate.swift
//  aviasales
//
//  Created by Станислав Сарычев on 31/07/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import UIKit
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private let _bag = DisposeBag()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        let navigationController = UINavigationController(nibName: nil, bundle: nil)
        window?.rootViewController = navigationController

        AppModule
            .make(
                mainModule: { [unowned self] in
                    self._mainModule(navigationController,
                                     searchModule: self._searchModule)()().map(MainModule.Output.asAppModuleInput)
                    },
                mapModule: { [unowned self] in
                    self._mapModule(navigationController)()($0).asEmpty()
            })()
            .subscribe()
            .disposed(by: _bag)

        window?.makeKeyAndVisible()
        return true
    }

    // MARK: - Modules
    private func _mainModule(_ navigationController: UINavigationController,
                             searchModule: @escaping (UINavigationController) -> () -> SearchModule.Start)
        -> () -> MainModule.Start {
            return MainModule
                .makeModule(show: { navigationController.pushViewController($0, animated: false) },
                            place: { title in
                                searchModule(navigationController)()(title).map(SearchModule.Output.asMainModuleInput)
                })
    }

    private func _searchModule(_ navigationController: UINavigationController) -> () -> SearchModule.Start {
        return SearchModule
            .makeModule(show: { navigationController.present(UINavigationController(rootViewController: $0),
                                                             animated: true) },
                        places: PlacesService.places)
    }

    private func _mapModule(_ navigationController: UINavigationController) -> () -> MapModule.Start {
        return MapModule
            .makeModule(show: { navigationController.pushViewController($0, animated: true) })
    }
}

extension MainModule.Output {
    static func asAppModuleInput(_ output: MainModule.Output) -> AppModule.Input {
        switch output {
        case .searchRequest(let request):
            return .searchRequest(request)
        }
    }
}

extension SearchModule.Output {
    static func asMainModuleInput(_ output: SearchModule.Output) -> MainModule.Input {
        switch output {
        case .place(let place):
            return .place(place)
        }
    }
}
