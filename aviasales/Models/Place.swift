//
//  Place.swift
//  aviasales
//
//  Created by Станислав Сарычев on 31/07/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

struct Place: Decodable, Equatable {
    let iata: String
    let location: Location
    let name: String
}
