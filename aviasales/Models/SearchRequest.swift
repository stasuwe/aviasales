//
//  SearchRequest.swift
//  aviasales
//
//  Created by Станислав Сарычев on 02/08/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

struct SearchRequest: Equatable {
    let from: Place
    let to: Place
}
