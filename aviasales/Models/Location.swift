//
//  Location.swift
//  aviasales
//
//  Created by Станислав Сарычев on 04/08/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

struct Location: Decodable, Equatable {
    let lat: Double
    let lon: Double
}
