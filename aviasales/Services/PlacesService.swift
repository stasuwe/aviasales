//
//  PlacesService.swift
//  aviasales
//
//  Created by Станислав Сарычев on 31/07/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import RxSwift

enum PlacesService {
    static func places(request: String) -> Single<[Place]> {
        guard let urlEncodedRequest = request.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let url = URL(string: "https://places.aviasales.ru/places?term=\(urlEncodedRequest)&locale=ru") else {
                return .never()
        }

        return URLSession.shared.rx
                .data(request: URLRequest(url: url))
                .map { try JSONDecoder().decode([Place].self, from: $0) }
                .asSingle()
    }
}
