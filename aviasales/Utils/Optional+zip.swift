//
//  Optional+zip.swift
//  aviasales
//
//  Created by Станислав Сарычев on 02/08/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

func zip<A, B>(_ a: A?, _ b: B?) -> (A, B)? {
    switch (a, b) {
    case (.some(let a), .some(let b)):
        return (a, b)
    default:
        return nil
    }
}
