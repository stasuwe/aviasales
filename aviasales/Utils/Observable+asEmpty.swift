//
//  Observable+asEmpty.swift
//  aviasales
//
//  Created by Станислав Сарычев on 04/08/2019.
//  Copyright © 2019 Станислав Сарычев. All rights reserved.
//

import RxSwift

extension Observable {
    func asEmpty<O>() -> Observable<O> {
        return flatMap { _ -> Observable<O> in .empty() }
    }
}
